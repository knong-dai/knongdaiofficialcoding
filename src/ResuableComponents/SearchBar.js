import React, { Component } from 'react';
import {View,TextInput,Image,StyleSheet} from 'react-native'
import {Button} from 'native-base'

class SearchBar extends Component {

    render() {
        return (
        <View style={[styles.searchBox]}>
          <View style={{flex:6,marginLeft:20}}>
            <TextInput placeholder='Search here...' />
          </View>
          <View style={{alignSelf:'flex-end',flex:1}}>
            <Button transparent>
              <Image source={require('../Image/icons8-search-90.png')} style={{ width: 25, height: 25}} />
            </Button>
          </View>
        </View>
        );
    }
}

export default SearchBar;

const styles=StyleSheet.create({
    searchBox:{
        flexDirection:'row',alignItems:'center',justifyContent:'center', 
        margin:10,borderRadius:50,marginTop:10,borderColor:'whitesmoke',
        borderWidth:3
      }
})