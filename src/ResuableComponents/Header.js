import React, { Component } from 'react';
import {View, Text,Image,StyleSheet,Platform} from 'react-native'
import {Container} from 'native-base'

class Header extends Component {

    render() {
        return (
            <View style={styles.container}>
                    <Text style={styles.humburger}
                    onPress={()=>this.props.navigation.goBack()}
                    >
                        <Image
                            style={{ width: 25, height: 25 }}
                            source={require('../Image/icons8-left-100-white.png')}
                        />
                    </Text>
                        <View style={{width:125,height:15,marginTop: 40}}>
                        <Image
                            style={{ width: '100%', height: '100%' }}
                            source={require('../Image/logo-kd-txt.png')}
                        />
                        </View>
                    
                    <View style={styles.notification}>
                        <Image
                            style={{ width: 25, height: 25 ,marginLeft:10}}
                            source={require('../Image/icons8-alarm-filled-100.png')}
                        />
                    </View>
                </View>
              
        );
    }
}

export default Header;


const styles = StyleSheet.create({
    container: {
         flexDirection: 'row',
        // justifyContent: 'space-around',
        height: (Platform.OS === 'ios') ? 150 : 30,
        backgroundColor: '#EE9A21',
        // alignItems: 'center',
        shadowColor: "#00f",
        paddingBottom: 100,
        marginBottom: 20,
        shadowOffset: { width: 0, height: 1 }
    },
    notification:{
        marginTop: Platform.OS='android'? 40:0 ,
        marginRight:-15
    },
    humburger:{
        marginTop: Platform.OS='android'? 40:0 ,
        marginLeft:-15
    }
})
