import React, { Component } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import {
  Container,
  Content,
  Form,
  Item,
  Label,
  Input,
  Card,
  CardItem,
  Icon,
  Right,
  Thumbnail,
  Button,
  Header,
  Left
} from "native-base";

import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";
export default class EditUser extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container>
        <Header />
        <Content>
          <Card style={styles.cardPhoto}>
            <Thumbnail
              large
              source={{
                uri:
                  "https://facebook.github.io/react-native/docs/assets/favicon.png"
              }}
              style={{ alignSelf: "center" }}
            />
          </Card>
          <Card style={{marginLeft:20,marginRight:20}}>
            <Form style={styles.frm}>
              <Item stackedLabel style={styles.all}>
                <Label style={Object.assign({}, styles.frm, styles.label)}>
                  User Name
                </Label>
                <Input
                  placeholder="Enter user name …"
                  style={Object.assign({}, styles.all, styles.input)}
                />
              </Item>
              <Text style={styles.gen}>Gender</Text>
              <RadioGroup style={styles.ratio}>
                <RadioButton value={"female"} style={styles.frm}>
                  <Text>Female</Text>
                </RadioButton>

                <RadioButton value={"male"} style={styles.frm}>
                  <Text>Male</Text>
                </RadioButton>
              </RadioGroup>

              <Item stackedLabel style={styles.all}>
                <Label style={Object.assign({}, styles.frm, styles.label)}>
                  Phone
                </Label>
                <Input
                  caretHidden
                  placeholder="Phone number …"
                  style={Object.assign({}, styles.all, styles.input)}
                />
              </Item>
              <Item style={styles.item}>
                <Left>
                  <Button block warning style={styles.btn}>
                    <Text>Cancel</Text>
                  </Button>
                </Left>
                <Right>
                  <Button block warning style={styles.btn}>
                    <Text>Save</Text>
                  </Button>
                </Right>
              </Item>
            </Form>
          </Card>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    // justifyContent: 'center',
    fontFamily: "Roboto"
  },
  title: {
    fontSize: 32,
    color: "#F7941D",
    marginBottom: 10
  },
  card: {
    marginTop: 20,
    borderColor: "#F7941D",
    borderWidth: 10
  },
  rigth: {
    marginRight: -60
  },
  itemtitle: {
    marginLeft: 15,
    fontSize: 16,
    marginTop: 10
  },
  item: {
    fontSize: 18,
    marginLeft: 30,
    marginTop: 5
  },
  buttom: {
    marginBottom: 10
  },
  cardPhoto: {
    marginTop: 10,
    borderColor: "#F7941D",
    borderWidth: 10,
    marginLeft: 20,
    marginRight: 20,
    height: 150,
    alignItems: "center",
    justifyContent: "center"
  },
  ratio: {
    flex: 1,
    flexDirection: "row",
    marginLeft: 20
  },
  frm: {
    fontSize: 16
  },
  btn: {
    fontFamily: "Roboto",
    fontSize: 16,
    width: 100,
    alignSelf: "center",
    marginBottom: 10,
    marginTop:10
  },
  all: {
    borderColor: "bisque",
    fontFamily: "Roboto",
    fontSize: 13
  },
  gen: {
    marginTop: 20,
    marginLeft: 10,
    fontFamily: "Roboto",
    fontSize: 16
  },
  label: {
    // marginLeft: 10
  },
  input: {
     marginRight: 10
  }
});
