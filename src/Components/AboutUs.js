import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Body, Text } from 'native-base';
import {StyleSheet} from 'react-native';
import ImageLogo from './image/imageLogo.js';
export default class AboutUS extends Component {
  render() {
    return (
      <Container style={{backgroundColor:'#F2F2F0'}}>
        <Header />
        <Content>
          <ImageLogo/>  
        <Text style={styles.myCardTitle}>Introduction</Text>
          <Card >
            <CardItem>
               
              <Body>
                <Text style={styles.introductionCard}>
                    Knong Dai is application service that
                    provides every users in powerful searching tool for Cambodian people. With Knong Dai application users can 
                    search for information within Cambodia by its category. 
                    Moreover, when you search everything in Knong Dai will 
                    suggested by any words
                </Text>
              </Body>
            </CardItem>
          </Card>


          <Text style={styles.myCardTitle}>Contact</Text>
          <Card>
            <CardItem>
               
              <Body>
                <Text style={styles.myStyle}>
                      <Text style={{ fontWeight: 'bold',fontSize:15,color:'#555555' }}>Address:    </Text>
                      <Text style={{marginLeft:20,fontSize:15,color:'#555555'}}>
                          #12, St 323, Sangkat Boeung Kak II,KhanToul Kork, Phnom Penh, Cambodia.
                      </Text>
                </Text>

                <Text style={styles.myStyle} style={{marginTop:5, fontSize:15,color:'#555555'}}>
                      <Text style={{ fontWeight: 'bold',marginLeft:10, fontSize:15,color:'#555555' }}>Tel:   </Text>
                        (855)23 991 314
                </Text>
                <Text style={styles.spaceBetween}> (855)77 77 12 36 (Khmer, English)</Text>
                <Text style={styles.spaceBetween}> (855)15 4 5555 2 (Khmer, English)</Text>
                <Text style={styles.spaceBetween}> (855)17 52 81 69 (Korean, English)</Text>
               
              </Body>
            </CardItem>
          </Card>
 
        </Content>

        
      </Container>
    );
  }
}
const styles=StyleSheet.create({
  myStyle:{
      // fontWeight: 'bold'
      fontSize:15,
      color:'#555555'

  },
  
  spaceBetween:{
      marginLeft:35,
      fontSize:15,
      color:'#555555'
  },

  myCardTitle:{
    color:'#3E7F83',
    fontSize:19,
    fontWeight: 'bold',
    marginTop:25,
    marginLeft:10

  },
  // myCard:{
  //   margin:5,
  // },

  introductionCard:{
    fontSize:15,
    color:'#555555'

  },

})