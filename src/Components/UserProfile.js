import React, { Component } from 'react';
import { View, Text,StyleSheet,Image } from 'react-native';
import {Container,Tabs,Tab,Card,CardItem,Icon,Right,Thumbnail, Button, Header} from 'native-base'

export default class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <Container>
            <Header/>
            <Card style={styles.cardPhoto}>
                <Thumbnail large source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}} style={{alignSelf:'center'}}/>
            </Card>
            <Tabs style={{marginLeft:20,marginRight:20,marginTop:10}}>
                <Tab heading="Personal Information">
                    <Card style={Object.assign({},styles.card,styles.title)}>
                        <Text style={styles.itemtitle}>Name</Text>
                        <Text style={styles.item}>Songden</Text>
                        <Text style={styles.itemtitle}>Gender</Text>
                        <Text style={styles.item}>Male</Text>
                        <Text style={styles.itemtitle}>Phone</Text>
                        <Text style={Object.assign({},styles.item,styles.buttom)}>0979207322</Text>
                    
                    </Card>   
                    <Button style={{alignSelf:'center',width:200,alignItems:'center',justifyContent:'center',backgroundColor:'#F7941D'}}>
                        <Text style={{alignSelf:'center',color:'#FFFFFF'}}>Edit</Text>
                    </Button>               
                </Tab>
                <Tab heading="History">
                    <Card style={styles.card}>
                        <CardItem>
                            <Text style={{color:'#3E7F83'}}>School</Text>
                            <Right style={styles.rigth}>
                                <Icon name="paper-plane" style={{color:'#3E7F83'}}/>
                            </Right>
                        </CardItem>
                        <CardItem>
                            <Text style={{color:'#3E7F83'}}>Hospital</Text>
                            <Right style={styles.rigth}>
                                <Icon name="paper-plane" style={{color:'#3E7F83'}}/>
                            </Right>
                        </CardItem>
                    </Card>
                </Tab>
            </Tabs>
        </Container>
    );
  }
}

const styles=StyleSheet.create({
    container:{
        flex:1,       
        alignItems: 'center', 
        justifyContent: 'center',  
        fontFamily:'Roboto'
    },
    title:{
        fontSize:32,
        color:'#F7941D',
        marginBottom: 10,
    },
    card:{
       marginTop: 20,
       borderColor: '#F7941D',
       borderWidth:10,
    },
    rigth:{
        marginRight:-60,
    },
    itemtitle:{
        marginLeft:15,
        fontSize:16,
        marginTop:10,
    },
    item:{
        fontSize:18,
        marginLeft:30,
        marginTop:5,
    },
    buttom:{
        marginBottom:10,
    },
    cardPhoto:{
        marginTop:10,
        borderColor: '#F7941D',
        borderWidth:10,
        marginLeft:20,
        marginRight:20,
        height:150,
        alignItems:'center',
        justifyContent:'center'
    },
})
