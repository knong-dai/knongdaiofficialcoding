import React, { Component } from 'react';
import { View, Text ,StyleSheet,Image,TextInput,Platform} from 'react-native';
import {Actions} from 'react-native-router-flux'
import { 
  Container, 
  Header, 
  Item, 
  Button,
  Card,
  CardItem,
  Left,
  Right,
  Body,
} from 'native-base';
import SearchBar from '../ResuableComponents/SearchBar';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Container>
        {/* Logo */}
        <View style={styles.container}>
                    <Text style={styles.humburger} onPress={()=> this.props.navigation.toggleDrawer()}>
                        <Image
                            style={{ width: 20, height: 25 }}
                            source={require('../Image/humburger.png')}
                        />
                    </Text>
                        <View style={{width:125,height:15,marginTop: 40}}>
                        <Image
                            style={{ width: '100%', height: '100%' }}
                            source={require('../Image/logo-kd-txt.png')}
                        />
                        </View>
                    
                    <Text style={styles.notification} 
                      onPress={() => this.props.navigation.navigate('Notification')}
                      >
                        <Image
                            style={{ width: 15, height: 20 ,marginLeft:10}}
                            source={require('../Image/icons8-alarm-filled-100.png')}
                        />
                    </Text>
                </View>
        <View style={styles.logo}>
          <Image source={require('../Image/logo-KD-small.png')} style={{ width: '100%', height: '100%'}} />
        </View>
        {/* Search bar */}
       <SearchBar/>
        {/* Body category */}
        <View style={[styles.shadow,styles.boxWrapper]}>
        {/* box cate */}
          <View style={[styles.fifty,styles.box]}>          
            <View>
              <Image 
              source={require('../Image/icons8-heart-with-pulse-100.png')} 
              style={{ width: 65, height: 65,}}  />
            </View>
            <View>
              <Text>Healthy</Text>
            </View> 
            <View style={{margin:-10}}>
              <Button transparent>
                <Image 
                source={require('../Image/icons8-expand-arrow-100.png')} 
                style={styles.btnDown} />
              </Button>
            </View>  
          </View>
          <View style={[styles.fifty,styles.box]}>          
            <View>
              <Image 
              source={require('../Image/icons8-heart-with-pulse-100.png')} 
              style={{ width: 65, height: 65,}}  />
            </View>
            <View>
              <Text>Healthy</Text>
            </View> 
            <View style={{margin:-10}}>
              <Button transparent>
                <Image 
                source={require('../Image/icons8-expand-arrow-100.png')} 
                style={styles.btnDown} />
              </Button>
            </View>  
          </View>
          <View style={[styles.fifty,styles.box]}>          
            <View>
              <Image 
              source={require('../Image/icons8-heart-with-pulse-100.png')} 
              style={{ width: 65, height: 65,}}  />
            </View>
            <View>
              <Text>Healthy</Text>
            </View>
            <View style={{margin:-10}}>
              <Button transparent>
                <Image 
                source={require('../Image/icons8-expand-arrow-100.png')} 
                style={styles.btnDown} />
              </Button>
            </View>
          </View>
          <View style={[styles.fifty,styles.box]}>          
            <View>
              <Image 
              source={require('../Image/icons8-heart-with-pulse-100.png')} 
              style={{ width: 65, height: 65,}}  />
            </View>
            <View>
              <Text>Healthy</Text>
            </View> 
            <View style={{margin:-10}}>
              <Button transparent>
                <Image 
                source={require('../Image/icons8-expand-arrow-100.png')} 
                style={styles.btnDown} />
              </Button>
            </View>  
          </View>
        </View>
        </Container>
    );
  }
}

const styles= StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#fff',
    flexDirection: 'column',
  },
  shadow:{
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
  },
  search:{
    padding:25,backgroundColor:'white',
  },
  logo:{
    width:100,height:100,alignSelf:'center',marginTop:20
  },
  fifty:{
    width:'48%'
  },
  boxWrapper:{
    flex:1,margin:5,marginTop:10,
    flexDirection:'row',
    flexWrap: 'wrap',
  },
  box:{
    backgroundColor:"#fff",
    justifyContent:'center',
    alignItems: 'center',
    height:180,
    margin:'1%',  
    borderColor:'whitesmoke',
    borderRadius:5,
    borderWidth:Platform.OS==='android'?3:0
  },
  btnDown:{
    width: 18, height: 18,borderColor:'#F7941D',borderWidth:1,borderRadius:9
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: (Platform.OS === 'ios') ? 80 : 30,
    backgroundColor: '#EE9A21',
    alignItems: 'center',
    shadowColor: "#00f",
    shadowOffset: { width: 0, height: 1 }
},
notification:{
    marginTop: Platform.OS='android'? 40:0 ,
    marginRight:-15
},
humburger:{
    marginTop: Platform.OS='android'? 40:0 ,
    marginLeft:-15
}
})
