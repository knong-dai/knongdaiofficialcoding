import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Body, Text } from 'native-base';
import {StyleSheet} from 'react-native';
import ImageLogo from './image/imageLogo.js';
export default class Policy extends Component {
  render() {
    return (
      <Container style={{backgroundColor:'#F2F2F0'}}>
        <Header />
        <Content>
          <ImageLogo/>  
        
          <Card >
            <CardItem>
              <Body>
              <Text style={styles.myCardTitle}>Thank you for using KNONG DAI! </Text>
                <Text style={styles.introductionCard}>
                        We wrote this policy to help you to understand what information we collect, 
                        how we use it and what choices you have. Because we are on company internet,
                        some of the concept below are a little technical, but we are tired out best 
                        explain things in simple and clear way. We welcome your questions and comments on this policy.
                </Text>
              </Body>
            </CardItem>
          </Card> 
        </Content>

        
      </Container>
    );
  }
}
const styles=StyleSheet.create({
  myStyle:{
      // fontWeight: 'bold'
      fontSize:15,
      color:'#555555'

  },
  
  spaceBetween:{
      marginLeft:35,
      fontSize:15,
      color:'#555555'
  },

  myCardTitle:{
    color:'#3E7F83',
    fontSize:19,
    fontWeight: 'bold',


  },
  myCard:{
   padding:5,
  
  },

 

})