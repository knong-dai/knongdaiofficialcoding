import React, { Component } from 'react';
import {createDrawerNavigator,createAppContainer,createStackNavigator} from 'react-navigation'
import {Image,Button,Text,StyleSheet,View} from 'react-native'
import Header from '../ResuableComponents/Header';
import Home from '../Components/Home'
import Notification from '../Components/Notification'
import SideMenu from './SideMenu'
import Setting from './Setting'

class MyHomeScreen extends React.Component {
    static navigationOptions = {
      drawerLabel: 'Home',
      drawerIcon: ({ tintColor }) => (
        <Image
          source={require('../Image/bell.png')}
          style={[styles.icon, {tintColor: tintColor}]}
        />
      ),
    };
  
    render() {
      return (
        <View>
          <Header/>
        <Button
          onPress={() => this.props.navigation.navigate('Notification')}
          title="Go to notifications"
        />
        <Text
         onPress={()=> this.props.navigation.toggleDrawer()}
        >
        Humburger
        </Text>
        </View>
      );
    }
  }
  
  class MyNotificationsScreen extends React.Component {
    static navigationOptions = {
      drawerLabel: 'Notifications',
      drawerIcon: ({ tintColor }) => (
        <Image
          source={require('../Image/bell.png')}
          style={[styles.icon, {tintColor: tintColor}]}
        />
      ),
    };
  
    render() {
      return (
        <Button
          onPress={() => this.props.navigation.goBack()}
          title="Go back home"
        />
      );
    }
  }
  
  const styles = StyleSheet.create({
    icon: {
      width: 24,
      height: 24,
    },
  });
  
  const MyDrawerNavigator = createDrawerNavigator({
    Home: {
      screen: Home,
    },
    Notification:{
      screen:Notification
    },
    Setting:{
      screen:Setting
    }
  },
  
  {
    contentComponent: SideMenu,
    drawerWidth: 300
  }
  );
  
 

  const MyApp = createAppContainer(MyDrawerNavigator);

  export default MyApp

  