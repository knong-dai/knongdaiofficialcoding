import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {Text, View,StyleSheet,Platform,Image,} from 'react-native'
import {ScrollView, Content,List,ListItem} from 'native-base';

export default class SideMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

render(){
    return(
        <View style={{ flex: 1 ,paddingTop:Platform.OS=='ios'?30:0,backgroundColor:'#F7941D'}}>
            <View 
                style={{ flex:1,justifyContent: 'center',padding:Platform.OS=='android'? 10:0 ,alignItems: 'center', backgroundColor:'#F7941D'}}>
                <View style={{width: 100, height: 100 ,backgroundColor:'#fff',borderRadius:50,
                  borderWidth:1,borderColor:'#fff'}}>
                  <Image source={require('../Image/kd-circle1.png')} 
                  style={{ width: "100%", height: "100%" ,}} 
                  />
                </View>
            </View>
            <View style={{ flex: 3,backgroundColor:'#fff',paddingTop:10 }}>
                <Content>
                    <List>
                        <ListItem onPress={()=>this.props.navigation.closeDrawer()}>
                              <Image 
                                source={require('../Image/icons8-home-filled-100.png')} 
                                style={styles.menuIcon}
                              />  
                              <Text 
                                
                                style={styles.menuText}
                              >HOME
                              </Text>
                        </ListItem>
                        <ListItem>
                              <Image 
                                source={require('../Image/icons8-user-100.png')} 
                                style={styles.menuIcon}
                              />
                              <Text style={styles.menuText}>USER PROFILE</Text>
                            </ListItem>
                            <ListItem>
                              <Image 
                                source={require('../Image/icons8-geography-filled-100.png')} 
                                style={styles.menuIcon}
                              />
                              <Text style={styles.menuText}>REQUEST WEBSITE</Text>
                            </ListItem>
                            <ListItem onPress={()=>this.props.navigation.navigate("Setting")}>
                              <Image 
                                source={require('../Image/icons8-services-480.png')} 
                                style={styles.menuIcon}
                              />
                              <Text                               
                                style={styles.menuText}
                                >SETTING
                              </Text>
                            </ListItem>
                            <ListItem>
                              <Image 
                                source={require('../Image/icons8-login-100.png')} 
                                style={styles.menuIcon}
                              />
                              <Text style={styles.menuText}>LOG IN</Text>
                            </ListItem>
                    </List>
                </Content>
            </View>
            
        </View>


    ) 
}


}

SideMenu.propTypes = {
    navigation: PropTypes.object
  };

const styles = StyleSheet.create({
    menuText:{
      color:'#555555',
      fontSize:13,
      fontWeight: 'bold',
    },
    menuIcon:{
      width:20,
      height:20,
      marginRight: 30
    }
  })