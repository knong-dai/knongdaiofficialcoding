import React, { Component } from 'react';
import { View, Text,StyleSheet, } from 'react-native';
import FormInput from './FormInput';
import {Card} from 'native-base'
export default class URLRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Card style={{marginLeft:10,marginRight:10,marginBottom:10}}>
          <Text style={styles.title}> Enter the website’s info </Text>
          <FormInput/>
        </Card>        
      </View>
    );
  }
}

const styles=StyleSheet.create({
    container:{
        flex:1,       
        alignItems: 'center',        
    },
    title:{
        fontSize:20,
        color:'#F7941D',
        fontFamily: 'Roboto',
        alignSelf: 'center',
        marginBottom: 10,
    }
})
