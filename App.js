/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import Main from './src/Components/Main';
import Home from './src/Components/Home'
import Result from './src/Components/Result'
import {createDrawerNavigator
,createStackNavigator,
createNavigationContainer
} from 'react-navigation'

export default class App extends Component {

  render() {
    return (
      <Main/>
    
    );
  }
}
const RootStack = createDrawerNavigator(
  {
    Home: Home,
    Details: Result,
  },
  {
  intialRouteName: 'Home',
  navigationOptions: {
    headerStyle : {
      backgroundColor: '#f4511e',
    },
    headerTintColor: '#fff',
    headerTitleStyle : {
      color: 'black',
      },
     },
   }
  );
  
  const AppStack = createNavigationContainer( RootStack);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});
